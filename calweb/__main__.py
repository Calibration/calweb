import os

import bottle

# NOTE: need to import this before pydarm.cmd.Report so that the
# CAL_ROOT path gets set appropriately
from .const import *

import pydarm
from pydarm.cmd import Report

##################################################

template_path = os.path.join(os.path.dirname(__file__), 'templates')
bottle.TEMPLATE_PATH.insert(0, template_path)

##################################################


app = bottle.Bottle()


@app.route("/report/<report_id>")
def report_route(report_id='last'):
    try:
        report = Report.find(report_id)
    except (ValueError, OSError):
        bottle.abort(404, f"Unknown report: {report_id}")

    return bottle.template(
        'report.tpl',
        report=report,
        IFO=IFO,
    )


def uncertainty_route(unc_time='list'):
    if unc_time == 'list':
        return bottle.template(
            'unc_list.tpl',
            IFO=IFO,
        )


@app.route("/")
def index():
    if 'report' in bottle.request.query:
        report_id = bottle.request.query.get('report')
        return report_route(report_id)

    elif 'uncertainty' in bottle.request.query:
        unc_time = bottle.request.query.get('uncertainty')
        return uncertainty_route(unc_time)

    return bottle.template(
        'index.tpl',
    )

##################################################

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    rgroup = parser.add_mutually_exclusive_group()
    rgroup.add_argument(
        '--server', '-s', choices=bottle.server_names, default='cgi',
        help="specify server type")
    rgroup.add_argument(
        '--run', action='store_true',
        help="run bottle internal server in debug mode")
    args = parser.parse_args()
    if args.run:
        kwargs = dict(
            debug=True,
            reload=True,
        )
    else:
        kwargs = dict(
            server=args.server,
        )
    app.run(**kwargs)
