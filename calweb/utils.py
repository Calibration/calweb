import os
import hashlib
from glob import glob as oglob

from gpstime import gpstime
from datetime import timezone

from pydarm.cmd import Report, Uncertainty
from pydarm.cmd.uncertainty import OFFSET_FROM_NOW
from pydarm.cmd._util import hex2int

from .const import *

##################################################

def glob(pathname, root_dir=None):
    if root_dir:
        paths = oglob(os.path.join(root_dir, pathname))
        return [os.path.relpath(path, root_dir) for path in paths]
    else:
        return oglob(pathname)


def get_file_contents(path, default=None, mode='r'):
    """return the contents of a file, or default if file doesn't exist

    """
    if os.path.exists(path):
        with open(path, mode) as f:
            return f.read()
    else:
        return default


def get_pydarm_version_html_from_file(path):
    version = get_file_contents(path)
    # FIXME: this assumes the reported pydarm corresponds directly to
    # a commit object, but it may in fact need some kind of
    # translation
    if version:
        url = f'https://git.ligo.org/Calibration/pydarm/-/tree/{version}'
        return f'<a href="{url}">{version}</a>'
    else:
        return f'?'


##############################


def uncertainty_display(unc):
    pydarm_url = get_pydarm_version_html_from_file(unc.gen_path('pydarm_version'))
    plot_path = unc.get_plot()
    now = gpstime.parse('now').replace(microsecond=0)
    data_gt = unc.get_data_datetime()
    data_gps = data_gt.gps()
    data_ago = now - data_gt.replace(microsecond=0)
    if data_ago.total_seconds() > 3600 + OFFSET_FROM_NOW + 120:
        data_color = 'Orange'
    else:
        data_color = 'MediumSeaGreen'
    file_gt = unc.get_gen_datetime().replace(microsecond=0)
    file_gps = file_gt.gps()
    file_ago = now - file_gt
    if file_ago.total_seconds() > 3600 + 120:
        file_color = 'Orange'
    else:
        file_color = 'MediumSeaGreen'
    return f"""
<p>generated {file_gt} <span style="color: {file_color}">{file_ago} ago</span></p>
<a href="{plot_path}"><img src="{plot_path}" style="width:75%" class="img-responsive" alt="{unc.gps} uncertainty estimate" /></a>
<br />
<table>
<tr><td align='right'>ID:     </td><td><a href="{unc.path}">{unc.gps}</a></td></tr>
<tr><td align='right'>Report: </td><td><a href="?report={unc.report}">{unc.report}:{unc.report_git_hash}</a></td></tr>
<tr><td align='right'>pyDARM version: </td><td>{pydarm_url}</td></tr>
<tr><td align='right'>Time:   </td><td>{data_gt} [GPS {data_gps}] <span style="color: {data_color}">{data_ago} ago</span></td></tr>
</table>
"""


def get_latest_uncertainty():
    path = os.path.join(UNCERTAINTY_DIR, 'latest')
    try:
        unc = Uncertainty(path)
    except Exception as e:
        return f"<b>Error processing latest uncertainty: {e}</b>"
    return uncertainty_display(unc)


def uncertainty_list_generator():
    root = UNCERTAINTY_DIR
    for epoch in sorted(os.listdir(root), reverse=True):
        if epoch == 'latest':
            continue
        edir = os.path.join(root, epoch)
        for esecs in sorted(os.listdir(edir), reverse=True):
            path = os.path.join(edir, esecs)
            try:
                unc = Uncertainty(path)
            except Exception as e:
                continue
            gps = unc.gps
            plot = unc.get_plot()
            report = unc.report
            yield f"""
<tr>
<div class="btn-group">
<td style='vertical-align: middle'><a href="{path}">{gps}</a></td>
<td style='vertical-align: middle'><a href="{plot}"><img src="{plot}" width=200></a></td>
<td style='vertical-align: middle'><a href="?report={report}">{report}</a></td>
</div>
</tr>
"""


##############################


def tag_button(tag):
    """HTML-formated tag"""
    for key in TAG_COLORS:
        if tag.startswith(key):
            break
    else:
        key = tag
    fg, bg = TAG_COLORS.get(key, TAG_COLORS['default'])
    return f'<span style="color:{fg}; background-color:{bg}">{tag}</span>'


def tag_buttons(tags):
    """HTML-formated tag list

    """
    if not tags:
        return ''
    return ' '.join([tag_button(tag) for tag in tags])


def get_gds_fir_file(report):
    return report.gen_path(f'gstlal_compute_strain_C00_filters_{IFO}.npz')


def sha256_hash_for_file(path):
    if os.path.exists(path):
        with open(path, 'rb') as f:
            fir_hash = hashlib.sha256(f.read()).hexdigest()
        fir_hash_int = hex2int(fir_hash)
        return f"{fir_hash} [{fir_hash_int}]"
    else:
        return """<b><span style="color: Red">file not found!</span></b>"""


def report_exported_html():
    try:
        report = Report(os.path.join(CAL_ROOT, 'reports', 'last-exported'))
    except FileNotFoundError:
        return '<span style="color: Red">Error: last exported report not found</span>'
    report_last_uploaded = Report.find(report.id)
    if report.git_status() != report_last_uploaded.git_status():
        vmsg = """<b><span style="color: Orange">NOTE: this does not correspond to the last uploaded version of this same report.</span></b>"""
    else:
        vmsg = ''
    tags = tag_buttons(report.tags)
    gps = report.id_int()
    git_hash, dirty = report.git_status()
    git_hash = git_hash[:7]
    git_int = report.git_int()
    gds_file = get_gds_fir_file(report)
    gds_file_info = sha256_hash_for_file(gds_file)
    return f"""
<table>
<tr><td align='right'>ID:                   </td><td><b><a href="?report={report.path}">{report.id}</a></b></td></tr>
<tr><td align='right'>GPS int:              </td><td><b>{gps}</b></td></tr>
<tr><td align='right'>git commit hash [int]:</td><td><b>{git_hash} [{git_int}]</b> {vmsg}</td></tr>
<tr><td align='right'>GDS file sha256 [int]:</td><td><b>{gds_file_info}</b></td></tr>
<tr><td align='right'>tags:                 </td><td>{tags}</td></tr>
</table>
"""


def report_table_row(ind, report):
    tags = tag_buttons(filter(lambda t: t!='valid', report.tags))
    valid = tag_buttons(filter(lambda t: t=='valid', report.tags))
    gps = report.id_int()
    pydarm_url = get_pydarm_version_html_from_file(report.gen_path('pydarm_version'))
    git_hash, dirty = report.git_status()
    git_hash = git_hash[:7]
    git_int = report.git_int()
    return f"""
<tr>
<div class="btn-group">
<td><span style="color:grey">{ind}</span></td>
<td><a href="?report={report.id}">{report.id}</a></td>
<td>{gps}</td>
<td>{git_hash} [{git_int}]</td>
<td>{pydarm_url}</td>
<td>{valid}</td>
<td>{tags}</td>
</div>
</tr>
"""


def get_report_sensing_plots(report):
    plots = [
        'sensing_tf_history.png',
        'sensing_mcmc_compare.png',
        'sensing_mcmc_corner.png',
        'sensing_gpr.png',
    ]
    return [report.gen_path(p) for p in plots]


def get_report_actuation_plots(report, stage):
    prefix = f'actuation_{stage}_*'
    plot_bases = [
        f'{prefix}_tf_history.png',
        f'{prefix}_mcmc_compare.png',
        f'{prefix}_mcmc_corner.png',
        f'{prefix}_gpr.png',
    ]
    plot_globs = [report.gen_path(p) for p in plot_bases]
    plots = []
    for pg in plot_globs:
        plots += glob(pg)
    return plots


def get_report_gds_filter_plots(report):
    return glob(report.gen_path('fir_plots', '*.png'))
