% import os

% import pydarm.cmd
% from pydarm.cmd import list_reports

% from calweb import utils
% from calweb.const import CAL_ROOT


% rebase('base.tpl', is_home=True)


<h4>latest uncertainty</h4>

% unc_html = utils.get_latest_uncertainty()
{{!unc_html}}
<br />


<h4><a href="?uncertainty=list">uncertainty history</a></h4>

<p>generated hourly   (<a href="pydarm_uncertainty.log">last log</a>)</p>
<img src="unc_hist.png" style="width:75%" class="img-responsive" alt="uncertainty history" />
<br />
<br />


<h4>last exported report</h4>

% report_exported_html = utils.report_exported_html()
{{!report_exported_html}}
<br />


<h4><a href="{{CAL_ROOT}}/reports">all reports</a></h4>

<div class="container">
<div class="col-md-12">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th></th>
<th>report ID</th>
<th>GPS</th>
<th>latest commit hash [int]</th>
<th>pydarm version</th>
<th>valid</th>
<th>tags</th>
</tr>
</thead>
<tbody>
% for ii, report in enumerate(list_reports(valid_only=False)):
%     report_html = utils.report_table_row(ii, report)
      {{!report_html}}
% end
</tbody>
</table>
</div>
</div>
