% from calweb.const import IFO, site, site_abrv, SITE, IFO_other, site_abrv_other

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{{IFO}} calibration</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
% if is_home:
    <meta http-equiv="refresh" content="30">
% end
<link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css" media="screen">
</head>
<body>
<div class="container">
<h2></h2>
<h2><a href="https://ldas-jobs.ligo-{{site_abrv}}.caltech.edu/~cal/">{{IFO}} calibration</a></h2>

<a href="https://git.ligo.org/groups/Calibration/-/wikis/home">wiki</a>
-
<a href="https://git.ligo.org/calibration/ifo/common">issue tracker</a>
-
<a href="https://online.ligo.org/">GWISTAT</a>
-
<a href="https://dashboard.igwn.org/">IGWN dashboard</a>
-
<a href="https://ldas-jobs.ligo-{{site_abrv_other}}.caltech.edu/~cal/">{{IFO_other}} calibration</a>
<br />

{{IFO}}:
<a href="https://git.ligo.org/calibration/ifo/{{IFO}}.git">config</a>
-
<a href="https://gstlal.ligo.caltech.edu/grafana/d/StZk6BPVz/calibration-monitoring?orgId=1&refresh=30s&var-DashDatasource={{site}}_calibration_monitoring_v3&var-strain_channel=GDS_STRAIN%22&var-coh_threshold=&var-detector_state=%22hoft_ok%22%20%3D%20%27HOFT_OK%27%20AND%20%22obs_intent%22%20%3D%20%27OBS_INTENT%27%20AND&from=now-24h&to=now">status/monitoring</a>
-
<a href="https://ldas-jobs.ligo-{{site_abrv}}.caltech.edu/~detchar/summary/today/cal/h_t_generation/">summary page</a>

<hr />

{{!base}}

</div>
<br />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script>
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
  });
</script>
</body>
</html>
