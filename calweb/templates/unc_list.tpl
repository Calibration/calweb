% import os

% from calweb import utils
% from calweb.const import UNCERTAINTY_DIR


% rebase('base.tpl', is_home=False)


<h4><a href="{{UNCERTAINTY_DIR}}">uncertainty</a></h4>

<div class="container">
<div class="col-md-12">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th>GPS</th>
<th>plot</th>
<th>report</th>
</tr>
</thead>
<tbody>

% for ii, unc_row in enumerate(utils.uncertainty_list_generator()):
    {{!unc_row}}
% end

</tbody>
</table>
</div>
</div>
