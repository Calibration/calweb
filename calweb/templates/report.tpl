% import os

% from calweb import utils


% rebase('base.tpl', is_home=False)

% raw_url = report.path
% pdf_url = report.report_file
% params_url = report.model_file
% tags = utils.tag_buttons(report.tags)
% pydarm_version_html = utils.get_pydarm_version_html_from_file(report.gen_path('pydarm_version'))
% gds_fir_path = utils.get_gds_fir_file(report)
% gds_fir_file = os.path.basename(gds_fir_path)
% gds_fir_info = utils.sha256_hash_for_file(gds_fir_path)
% gt = report.id_gpstime()
% utc = gt
% gps = report.id_int()
% git_hash, dirty = report.git_status()
% git_hash = git_hash[:7]
% git_int = report.git_int()
% gstring = os.path.join(report.path, '*.png')
% # plots = glob(gstring)

<h3>Report: <a href="{{raw_url}}">{{report.id}}</a> (<a href="{{pdf_url}}">pdf</a>)</h3>
<h6>UTC: {{utc}}</h6>
<h6>GPS: {{gps}}</h6>
<h6>git hash: {{git_hash}} [{{git_int}}]</h6>
<h6>pydarm version: {{!pydarm_version_html}}</h6>
<h6>tags: {{!tags}}</h6>
<h6>model params: <a href="{{params_url}}">pydarm_{{IFO}}.ini</a></h6>
<h6>GDS FIR file: <a href="{{gds_fir_path}}">{{gds_fir_file}}</a>
<h6>GDS FIR sha256sum [int]: {{gds_fir_info}}

<hr />

<h4>sensing function</h4>

% for plot in utils.get_report_sensing_plots(report):
<a href="{{plot}}"><img src="{{plot}}" style="width:100%"
			class="img-responsive" alt="{{plot}}" /></a>
<br />
% end

<h4>actuation functions</h4>

<h5>actuation function: L1 (UIM) stage</h5>

% for plot in utils.get_report_actuation_plots(report, 'L1'):
<a href="{{plot}}"><img src="{{plot}}" style="width:100%"
			class="img-responsive" alt="{{plot}}" /></a>
<br />
% end

<h5>actuation function: L2 (PUM) stage</h5>

% for plot in utils.get_report_actuation_plots(report, 'L2'):
<a href="{{plot}}"><img src="{{plot}}" style="width:100%"
			class="img-responsive" alt="{{plot}}" /></a>
<br />
% end

<h5>actuation function: L3 (TST) stage</h5>

% for plot in utils.get_report_actuation_plots(report, 'L3'):
<a href="{{plot}}"><img src="{{plot}}" style="width:100%"
			class="img-responsive" alt="{{plot}}" /></a>
<br />
% end

<h5>GDS filters</h5>

% for plot in utils.get_report_gds_filter_plots(report):
<a href="{{plot}}"><img src="{{plot}}" style="width:100%"
			class="img-responsive" alt="{{plot}}" /></a>
<br />
% end
