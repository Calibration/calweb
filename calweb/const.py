import os


IFO = os.getenv('IFO')
if not IFO:
    exit('Must specify IFO env var.')
site, site_abrv, IFO_other, site_abrv_other = {
    'H1': ('lho', 'wa', 'L1', 'la'),
    'L1': ('llo', 'la', 'H1', 'wa'),
}[IFO]
SITE = site.upper()


WEB_ROOT = os.getenv('WEB_ROOT', '')

CAL_ROOT = f'archive/{IFO}'
os.environ['CAL_ROOT'] = CAL_ROOT

UNCERTAINTY_DIR = os.path.join(CAL_ROOT, 'uncertainty', 'v0')


# foreground, background
TAG_COLORS = {
    'default': ('white', 'grey'),
    'valid': ('white', 'green'),
    'exported': ('white', 'purple'),
    'epoch': ('black', 'orange'),
}
