#!/bin/bash
source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
conda activate igwn
source /home/cal/opt/pydarm-dev/bin/activate
set -a
source /home/cal/config
export PYTHONPATH=/home/cal/src/calweb
export MPLCONFIGDIR=/tmp/
exec python -m calweb "$@"
